package fr.iut;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class StringUtil {

    /**
     * Transform an amount of money in the a displayable value of that amount with the corresponding currency.
     *
     * @param amount of money
     * @param locale the nationality of the currency needed
     * @return a displayable amount of money
     */
    public static String prettyCurrencyPrint(final double amount, final Locale locale) {
       String c = NumberFormat.getCurrencyInstance(locale).getCurrency().getSymbol();
       DecimalFormat f = new DecimalFormat();
       f.setMaximumFractionDigits(2);
       return String.format("%s %s", f.format(amount), c);
    }
}
