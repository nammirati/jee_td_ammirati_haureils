package bean;

import org.hibernate.validator.constraints.*;
import validation.Login;
import javax.validation.constraints.*;

public class Person {

    @NotNull @Email(message = "email incorrect format")
    private String email;
    @NotNull
    private String firstName, lastName;
    @NotNull @Login
    private String login;
    @NotNull
    private boolean isStudent;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isStudent() {
        return isStudent;
    }

    public void setStudent(boolean student) {
        isStudent = student;
    }
}
