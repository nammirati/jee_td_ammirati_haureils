package fr.iut;

import org.junit.Test;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;

import static org.junit.Assert.*;

public class StringUtilTest {


    @Test
    public void testInstance(){
        StringUtil su = new StringUtil();
        assertTrue(su != null);
    }
    @Test
    public void testPoly(){
        assertTrue(StringUtil.prettyCurrencyPrint(21500.390, Locale.FRANCE).equals("21 500,39 €"));
    }

    @Test
    public void testZero(){
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.FRANCE).equals("0 €"));
    }

    @Test
    public void testRandomAmount(){
        Random r = new Random();
        DecimalFormat f = new DecimalFormat();
        f.setMaximumFractionDigits(2);
        for (int i  = 0; i <= 100; i++) {
            double d = r.nextDouble();
            assertTrue(StringUtil.prettyCurrencyPrint(d, Locale.FRANCE).equals(String.format("%s €", f.format(d))));
        }
    }

    @Test
    public void testCurrencies(){
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.FRANCE).equals("0 €"));
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.UK).equals("0 GBP"));
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.US).equals("0 USD"));
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.JAPAN).equals("0 JPY"));
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.CANADA).equals("0 CAD"));
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.CHINA).equals("0 CNY"));
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.GERMANY).equals("0 €"));
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.ITALY).equals("0 €"));
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.KOREA).equals("0 KRW"));
        assertTrue(StringUtil.prettyCurrencyPrint(0, Locale.TAIWAN).equals("0 TWD"));
    }
}