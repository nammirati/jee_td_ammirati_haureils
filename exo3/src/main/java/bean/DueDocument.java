package bean;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class DueDocument extends Document {

    @NotNull
    private Date dueDate;

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
}
