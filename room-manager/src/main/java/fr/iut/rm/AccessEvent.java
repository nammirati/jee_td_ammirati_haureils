package fr.iut.rm;

import fr.iut.rm.persistence.domain.Room;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

public class AccessEvent {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String name;
    /**
     * true = entrée
     * false = sortie
     */
    @Column(nullable = false)
    private boolean eventType;

    @Column(nullable = false)
    private Date date;

    @ManyToOne
    private Room room;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEventType() {
        return eventType;
    }

    public void setEventType(boolean eventType) {
        this.eventType = eventType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
