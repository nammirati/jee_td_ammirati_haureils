package fr.iut.rm.control;

import com.google.inject.Inject;
import com.google.inject.persist.UnitOfWork;
import fr.iut.rm.AccessEvent;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.dao.impl.RoomDaoImpl;
import fr.iut.rm.persistence.domain.Room;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by cjohnen on 02/02/17.
 */
public class ControlRoom {
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ControlRoom.class);

    /**
     * Unit of work is used to drive DB Connection
     */
    @Inject
    UnitOfWork unitOfWork;

    /**
     * Data Access Object for rooms
     */
    @Inject
    RoomDao roomDao;

    static HashMap<Room, ArrayList<AccessEvent>> listAE = new HashMap<Room, ArrayList<AccessEvent>>();

    /*
     * * Displays all the rooms content in DB
     */
    public void showRooms() {
        unitOfWork.begin();

        List<Room> rooms = roomDao.findAll();
        if (rooms.isEmpty()) {
            System.out.println("No room");
        } else {
            System.out.println("Rooms :");
            System.out.println("--------");
            for (Room room : rooms) {
                System.out.println(String.format("   [%d], name '%s', description '%s'", room.getId(), room.getName(), room.getDescription()));
            }
        }

        unitOfWork.end();
    }

    /**
     * Creates a room in DB
     *
     * @param name the name of the room
     */
    public void createRoom(final String name, final String description) {
        unitOfWork.begin();

        // TODO check unicity

        Room room = new Room();
        room.setName(name);
        room.setDescription(description);
        roomDao.saveOrUpdate(room);
        unitOfWork.end();
    }

    public void removeRoom(final String name) {
        unitOfWork.begin();
        roomDao.removeRoom(name);
        unitOfWork.end();
    }

    public void enterRoom(final String name) {
        unitOfWork.begin();
        AccessEvent accEvent = new AccessEvent();
        Room room = roomDao.findByName(name);
        accEvent.setRoom(room);
        Date date = new Date();
        accEvent.setDate(date);
        accEvent.setEventType(true);
        accEvent.setName(String.format("entered %s at %s", name, date.toString()));
        System.out.println(accEvent.getName());
        if (!listAE.containsKey(room)) {
            listAE.put(room, new ArrayList<AccessEvent>());
        }
        listAE.get(room).add(accEvent);
        unitOfWork.end();
    }

    public void getOutOfRoom(final String name) {
        unitOfWork.begin();
        AccessEvent accEvent = new AccessEvent();
        Room room = roomDao.findByName(name);
        accEvent.setRoom(room);
        Date date = new Date();
        accEvent.setDate(date);
        accEvent.setEventType(false);
        accEvent.setName(String.format("got out of %s at %s", name, date.toString()));
        System.out.println(accEvent.getName());
        if (!listAE.containsKey(room)) {
            listAE.put(room, new ArrayList<AccessEvent>());
        }
        listAE.get(room).add(accEvent);
        unitOfWork.end();
    }

    public static HashMap<Room, ArrayList<AccessEvent>> getListAE() {
        return listAE;
    }
}
