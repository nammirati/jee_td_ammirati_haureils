package fr.iut.club;

import fr.iut.Ball;
import fr.iut.Club;

import java.awt.geom.Point2D;

public class PutterExperimental implements Club {

    public void shoot(double force, double direction, Ball ball) {
        ball.setPosition(new Point2D.Double(ball.getPosition().getX() + 10, ball.getPosition().getY() + 10));
    }
}
