package fr.iut;

import com.google.inject.servlet.ServletModule;


public class Module extends ServletModule {

    @Override
    protected void configureServlets() {
        super.configureServlets();
        serve("/form").with(Formulaire.class);
        serve("/home").with(HomeServlet.class);
        serve("/hello").with(HelloServlet.class);
        serve("/list").with(ListServlet.class);
    }
}
